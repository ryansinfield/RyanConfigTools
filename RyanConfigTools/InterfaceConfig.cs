﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace RyanConfigTools
{
    public class InterfaceConfig
    {
        #region Fields
        private KeyValueConfigurationCollection config;
        private DateTime startTime;
        private Dictionary<string, List<string>> successfulTransactions;
        private Dictionary<string, List<string>> failedTransactions;
        private const string dtFilenameFormat = "dd-MM-yyyy_HH-mm-ss";
        private const string dtSqlFormat = "yyyy-MM-dd HH:mm:ss";
        private const string dtSqlFormatDateOnly = "yyyy-MM-dd";
        private string separator;
        #endregion

        #region Properties
        public DateTime StartTime { get => startTime; }
        public Dictionary<string, List<string>> SuccessfulTransactions { get => successfulTransactions; }
        public Dictionary<string, List<string>> FailedTransactions { get => failedTransactions; }
        public static string DateTimeFilenameFormat => dtFilenameFormat;
        public static string DateTimeSqlFormat => dtSqlFormat;
        public static string DateTimeSqlFormatDateOnly => dtSqlFormatDateOnly;

        // Access config variables
        public string this[params string[] varComponents]
        {
            get
            {
                return GetFromConfigFile(varComponents);
            }
        }
        #endregion

        #region Constructors
        /// <summary>
        /// Instantiate a new InterfaceConfig object
        /// </summary>
        /// <param name="config">The KeyValueConfigurationCollection object containing config data</param>
        /// <param name="environment">The config key prefix for this environment (if applicable), e.g. LIVE or TEST</param>
        public InterfaceConfig(KeyValueConfigurationCollection config, string separator="::")
        {
            this.config = config;
            this.separator = separator;
            startTime = DateTime.Now;
            successfulTransactions = new Dictionary<string, List<string>>();
            failedTransactions = new Dictionary<string, List<string>>();
        }
        #endregion

        #region Methods
        /// <summary>
        /// Retrieve a configuration value
        /// </summary>
        /// <param name="varComponents">The components of the config variable name</param>
        /// <returns>The configuration value as a string</returns>
        private string GetFromConfigFile(params string[] varComponents)
        {
            if (varComponents.Length < 1) throw new InterfaceException("No variable components were specified.");
            string varName = String.Join(separator, varComponents);

            try
            {
                return config[varName].Value;
            }
            catch (NullReferenceException ex)
            {
                throw new InterfaceException($"The requested configuration variable '{varName}' has not been set.", ex);
            }
        }

        /// <summary>
        /// Convert a dictionary to a log description string
        /// </summary>
        /// <param name="logInfo">A dictionary containing log information</param>
        /// <returns>A formatted log description string</returns>
        public string GenerateLogDescription(Dictionary<string, string> logInfo)
        {
            string description = "";
            foreach (KeyValuePair<string, string> kv in logInfo)
            {
                description = description + $"<b>{kv.Key}:</b> {kv.Value}<br/>";
            }
            return description;
        }

        /// <summary>
        /// Add a successful transaction
        /// </summary>
        /// <param name="key">The key to store the successful transaction description</param>
        /// <param name="description">Description of the transaction</param>
        public void AddSuccessful(string key, Dictionary<string, string> description)
        {
            AddTransactionLog(key, description, successfulTransactions);
        }

        /// <summary>
        /// Add a failed transaction
        /// </summary>
        /// <param name="key">The key to store the failed transaction</param>
        /// <param name="description">Description of the transaction</param>
        public void AddFailed(string key, Dictionary<string, string> description)
        {
            AddTransactionLog(key, description, failedTransactions);
        }

        /// <summary>
        /// Add a transaction log description to a dictionary
        /// </summary>
        /// <param name="key">The category key of the transaction</param>
        /// <param name="logInfo">Log information</param>
        /// <param name="dict">The dictionary to update</param>
        private void AddTransactionLog(string key, Dictionary<string, string> logInfo, Dictionary<string, List<string>> dict)
        {
            string description = GenerateLogDescription(logInfo);
            if (!dict.ContainsKey(key))
            {
                dict[key] = new List<string>();
            }
            dict[key].Add(description);
        }
        #endregion
    }
}
