﻿using System;
using System.Runtime.Serialization;

namespace RyanConfigTools
{
    [Serializable]
    public class InterfaceException : System.Exception
    {
        public InterfaceException()
            : base() { }

        public InterfaceException(string message)
            : base(message) { }

        public InterfaceException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public InterfaceException(string message, System.Exception innerException)
            : base(message, innerException) { }

        public InterfaceException(string format, System.Exception innerException, params object[] args)
            : base(string.Format(format, args), innerException) { }

        public InterfaceException(SerializationInfo info, StreamingContext context)
            : base(info, context) { }
    }
}
