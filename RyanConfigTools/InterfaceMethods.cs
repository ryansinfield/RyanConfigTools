﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace RyanConfigTools
{
    public class InterfaceMethods
    {
        /// <summary>
        /// Invoke a static class' static method from strings
        /// </summary>
        /// <param name="className">The name of the class</param>
        /// <param name="methodName">The name of the method</param>
        /// <param name="methodArguments">An object array containing method arguments</param>
        public static void InvokeFromString(string assemblyName, string className, string methodName, object[] methodArguments)
        {
            Assembly a = Assembly.Load(assemblyName);
            Type t = a.GetType($"{assemblyName}.{className}");
            if (t == null) throw new InterfaceException($"Type {className} could not be found.");
            MethodInfo mth = t.GetMethod(methodName);
            mth.Invoke(null, methodArguments);
        }
    }
}
